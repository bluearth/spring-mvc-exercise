package exercise.agenda.dao;

import org.springframework.stereotype.Repository;

import exercise.agenda.model.Person;

@Repository
public class PersonDAO extends BaseDAO<Long, Person>{

	public PersonDAO() {
		super(Long.class, Person.class);
	}
	
}
