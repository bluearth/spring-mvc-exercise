package exercise.agenda.dao;

import org.springframework.stereotype.Repository;

import exercise.agenda.model.Calendar;

@Repository
public class CalendarDAO extends BaseDAO<Long, Calendar> {

	public CalendarDAO() {
		super(Long.class, Calendar.class);
	}
	

}
