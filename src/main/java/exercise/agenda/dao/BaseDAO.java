package exercise.agenda.dao;

import java.io.Serializable;
import java.util.Collection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseDAO<ID extends Serializable, T> {

	@Autowired
	SessionFactory sessionFactory;
	
	Class<T> itemType;
	
	Class<ID> idType;
	
	public BaseDAO(Class<ID> idType, Class<T> itemType) {
		this.idType = idType;
		this.itemType = itemType;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void	 setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	final Session session(){
		return this.sessionFactory.getCurrentSession();
	}
	
	public Collection<T> findAll(){
		return session().createCriteria(this.itemType)
				.list();
	}
	
	public T create(T item){
		session().save(item);
		return item;
	}
	
	public Collection<T> findByProperty(String propertyName, String filterValue){
		return session().createCriteria(this.itemType)
				.add(Restrictions.eq(propertyName, filterValue))
				.list();
	}

	public T getByProperty(String propertyName, String filterValue){
		return (T) session().createCriteria(this.itemType)
				.add(Restrictions.eq(propertyName, filterValue))
				.uniqueResult();
	}
	
}
