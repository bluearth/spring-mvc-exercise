package exercise.agenda.dao;

import org.springframework.stereotype.Repository;

import exercise.agenda.model.Event;

@Repository
public class EventDAO extends BaseDAO<Long, Event>{

	public EventDAO() {
		super(Long.class, Event.class);
	}

}
