package exercise.agenda.sec.dao;

import org.springframework.stereotype.Repository;

import exercise.agenda.dao.BaseDAO;
import exercise.agenda.sec.model.User;

@Repository
public class UserDao extends BaseDAO<Long, User> {
	
	public UserDao(){
		super(Long.class, User.class);
	}

}
