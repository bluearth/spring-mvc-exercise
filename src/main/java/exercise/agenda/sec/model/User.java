package exercise.agenda.sec.model;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name="Sec_User")
public class User implements UserDetails{

	Long id;
	
	String username;
	
	String password;

	boolean accountNotExpired;
	
	boolean accountNotLocked;
	
	boolean passwordNotExpired;
	
	boolean enabled;
	
	Collection<Right> rights;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator")
	@GenericGenerator(name = "hilo_sequence_generator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "SeqUser_userId"),
			@Parameter(name = "initial_value", value = "10"), @Parameter(name = "increment_size", value = "1"),
			@Parameter(name = "optimizer", value = "hilo") })	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isAccountNotExpired() {
		return accountNotExpired;
	}

	public void setAccountNotExpired(boolean accountNotExpired) {
		this.accountNotExpired = accountNotExpired;
	}

	public boolean isAccountNotLocked() {
		return accountNotLocked;
	}

	public void setAccountNotLocked(boolean accountNotLocked) {
		this.accountNotLocked = accountNotLocked;
	}

	public boolean isPasswordNotExpired() {
		return passwordNotExpired;
	}

	public void setPasswordNotExpired(boolean passwordNotExpired) {
		this.passwordNotExpired = passwordNotExpired;
	}

	@Override
	@NotNull
	@Size(min=1, max=300)
	public String getUsername() {
		return this.username;
	}	

	public void setUsername(String username) {
		this.username = username;
	}
	
	@Override
	@NotNull
	@Size(min=1, max=300)
	public String getPassword() {
		return this.password;
	}	

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@ManyToMany(fetch=FetchType.EAGER) // <-- should've been 'LAZY'
	@JoinTable(name="Sec_UserRights"
			, joinColumns= @JoinColumn(name="UserId")
			, inverseJoinColumns=@JoinColumn(name="RightId", unique=false))
	public Collection<Right> getRights(){
		return this.rights;
	}
	
	public void setRights(Collection<Right> rights){
		this.rights = rights;
	}
	
	//
	// Some UserDetails specific methods, not to be persisted
	//
	@Override
	@Transient
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return getRights();
	}

	@Override
	@Transient
	public boolean isAccountNonExpired() {
		return isAccountNotExpired();
	}

	@Override
	@Transient
	public boolean isAccountNonLocked() {
		return isAccountNotLocked();
	}

	@Override
	@Transient
	public boolean isCredentialsNonExpired() {
		return isPasswordNotExpired();
	}
}
