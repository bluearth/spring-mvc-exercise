package exercise.agenda.sec.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name="Sec_Right")
public class Right implements GrantedAuthority {

	Long id;
	
	String rightName;
	
	String description;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hilo_sequence_generator")
	@GenericGenerator(name = "hilo_sequence_generator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "SeqRight_rightId"),
			@Parameter(name = "initial_value", value = "10"), @Parameter(name = "increment_size", value = "1"),
			@Parameter(name = "optimizer", value = "hilo") })
	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}
	
	@NotNull
	@Size(min=1, max=300)
	public String getRightName() {
		return rightName;
	}

	public void setRightName(String rightName) {
		this.rightName = rightName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	//
	// Some GrantedAuthority specific methods, not to be persisted
	//
	@Override
	@Transient
	public String getAuthority() {
		return rightName;
	}

}
