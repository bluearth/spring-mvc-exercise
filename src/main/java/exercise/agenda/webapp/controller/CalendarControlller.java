package exercise.agenda.webapp.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import exercise.agenda.model.Calendar;
import exercise.agenda.service.AgendaService;

@Controller
@RequestMapping(path="/calendar")
public class CalendarControlller {

	@Autowired
	AgendaService agendaService;
	
	@RequestMapping(path="/")
	public String index(Model model){
		Collection<Calendar> calendars = agendaService.findAllCalendar();
		model.addAttribute("calendars", calendars);
		return "calendar/index";
	}
	

	
	
	
}
