package exercise.agenda.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path = "/", method = RequestMethod.GET)
public class HomeController {

	@RequestMapping
	public ModelAndView index(){
		return new ModelAndView("index", "message", "Welcome to Spring MVC!");
	}
}
