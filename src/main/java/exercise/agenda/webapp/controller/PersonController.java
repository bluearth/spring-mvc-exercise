package exercise.agenda.webapp.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import exercise.agenda.model.Person;
import exercise.agenda.service.AgendaService;

@Controller
@RequestMapping(path="/person")
public class PersonController extends BaseController {

	@Autowired
	AgendaService agendaService;
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView index(){
		Collection<Person> items = agendaService.findAllPerson();		
		return new ModelAndView("person/index")
				.addObject("items", items);
	}

	@RequestMapping(method=RequestMethod.POST)
	public ModelAndView add(@Valid @ModelAttribute Person p, BindingResult bindingResult){
	
		if (!bindingResult.hasErrors()) {
			agendaService.createPerson(p);
		}
		
		return index()
				.addObject("newItem", p)
				.addObject("bindingResult", bindingResult);
	}	
}
