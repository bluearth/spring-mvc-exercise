package exercise.agenda.webapp;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages="exercise.agenda")
@PropertySource("classpath:/config.properties")
@EnableTransactionManagement
@ImportResource("classpath:/application-context.xml")
public class WebAppConfig {

	@Autowired
	Environment env;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Bean
	public DataSource dataSource(){
		BasicDataSource dbcpDatasource = new BasicDataSource();
		dbcpDatasource.setDriverClassName(env.getProperty("db.driver"));
		dbcpDatasource.setUrl(env.getProperty("db.url"));
		dbcpDatasource.setUsername("");
		dbcpDatasource.setPassword("");
		return dbcpDatasource;
	}

	@Bean(name="sessionFactory")
	public LocalSessionFactoryBean localSessionFactoryBean() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		
		Properties hibernateProperties = new Properties();
		hibernateProperties.setProperty("hibernate.format_sql", env.getProperty("hibernate.format_sql"));
		hibernateProperties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
		hibernateProperties.setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
		sessionFactory.setHibernateProperties(hibernateProperties);
		sessionFactory.setPackagesToScan(
				  "exercise.agenda.model"
				, "exercise.agenda.sec.model");
		return sessionFactory;
	}

	@Bean
	public HibernateTransactionManager transactionmanager(){
		return new HibernateTransactionManager(localSessionFactoryBean().getObject());
	}
	
	@Bean
	public ViewResolver urlBasedViewResolver(){
		UrlBasedViewResolver viewResolver = new UrlBasedViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/jsp/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}
	
}
