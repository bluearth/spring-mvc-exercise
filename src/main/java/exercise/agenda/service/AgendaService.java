package exercise.agenda.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import exercise.agenda.dao.CalendarDAO;
import exercise.agenda.dao.EventDAO;
import exercise.agenda.dao.PersonDAO;
import exercise.agenda.model.Calendar;
import exercise.agenda.model.Person;

@Service
@Transactional
public class AgendaService {

	@Autowired
	PersonDAO personDao;
	
	@Autowired
	CalendarDAO calendarDao;
	
	@Autowired
	EventDAO  eventDao;

	@Transactional(readOnly=true)
	public Collection<Person> findAllPerson(){
		return personDao.findAll();
	}
	
	public Person createPerson(Person p){
		return personDao.create(p);
	}
	
	@Transactional(readOnly=true)
	public Collection<Calendar> findAllCalendar(){
		return calendarDao.findAll();
	}
	
	
}
