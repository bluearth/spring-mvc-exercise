
insert into Person(id, name, dob) values (1, 'Andi Rahman Yusuf', '1991-04-03 00:00:00.000');
insert into Person(id, name, dob) values (2, 'Fidia Rahmawati', '1988-06-14 00:00:00.000');
insert into Person(id, name, dob) values (3, 'Muhammad Effendi Salim', '1989-02-11 00:00:00.000');
insert into Person(id, name, dob) values (4, 'Yanti Dwi Hapsari', '1975-11-05 00:00:00.000');
insert into Person(id, name, dob) values (5, 'Zildan Herstein', '1980-09-25 00:00:00.000');

insert into Calendar (id, title, ownerId) values (1, 'Andi''s calendar', 1);
insert into Calendar (id, title, ownerId) values (2, 'Fidia''s calendar', 2);
insert into Calendar (id, title, ownerId) values (3, 'Muhammad''s calendar', 3);
insert into Calendar (id, title, ownerId) values (4, 'Yanti''s calendar', 4);
insert into Calendar (id, title, ownerId) values (5, 'Zildan''s calendar', 5);

-- BEGIN Enable this if you are using table-per-hierarchy inhertance mapping strategy --
--insert into Event (id, recurring, period, title, description, startTime, endTime, calendar) values (1, false, null, 'Cuti umum', '', '2015-10-20 00:00:00.000', '2015-10-21 00:00:00.000', 2);
--insert into Event (id, recurring, period, title, description, startTime, endTime, calendar) values (2, false, null, 'Cuti umum', '', '2015-11-04 00:00:00.000', '2015-11-06 00:00:00.000', 4);
--insert into Event (id, recurring, period, title, description, startTime, endTime, calendar) values (3, true, 5, 'Andi''s birthday', '', '1991-04-03 00:00:00.000', '1991-04-03 00:00:00.000', 1);
--insert into Event (id, recurring, period, title, description, startTime, endTime, calendar) values (4, true, 5, 'Fidia''s birthday', '', '1988-06-14 00:00:00.000', '1988-06-14 00:00:00.000', 2);
--insert into Event (id, recurring, period, title, description, startTime, endTime, calendar) values (5, true, 5, 'Muhammad''s birthday', '', '1989-02-11 00:00:00.000', '1989-02-11 00:00:00.000', 3);
--insert into Event (id, recurring, period, title, description, startTime, endTime, calendar) values (6, true, 5, 'Yanti''s birthday', '', '1975-11-05 00:00:00.000', '1975-11-05 00:00:00.000', 4);
--insert into Event (id, recurring, period, title, description, startTime, endTime, calendar) values (7, true, 5, 'Zildan''s birthday', '', '1980-09-25 00:00:00.000', '1980-09-25 00:00:00.000', 5);
-- END

-- BEGIN Enable this if you are using table-per-subclass inhertance mapping strategy --
--insert into Event (id, title, description, startTime, endTime, calendar) values (1, 'Cuti umum', '', '2015-10-20 00:00:00.000', '2015-10-21 00:00:00.000', 2);
--insert into Event (id, title, description, startTime, endTime, calendar) values (2, 'Cuti umum', '', '2015-11-04 00:00:00.000', '2015-11-06 00:00:00.000', 4);
--insert into Event (id, title, description, startTime, endTime, calendar) values (3, 'Andi''s birthday', '', '1991-04-03 00:00:00.000', '1991-04-03 00:00:00.000', 1);
--insert into Event (id, title, description, startTime, endTime, calendar) values (4, 'Fidia''s birthday', '', '1988-06-14 00:00:00.000', '1988-06-14 00:00:00.000', 2);
--insert into Event (id, title, description, startTime, endTime, calendar) values (5, 'Muhammad''s birthday', '', '1989-02-11 00:00:00.000', '1989-02-11 00:00:00.000', 3);
--insert into Event (id, title, description, startTime, endTime, calendar) values (6, 'Yanti''s birthday', '', '1975-11-05 00:00:00.000', '1975-11-05 00:00:00.000', 4);
--insert into Event (id, title, description, startTime, endTime, calendar) values (7, 'Zildan''s birthday', '', '1980-09-25 00:00:00.000', '1980-09-25 00:00:00.000', 5);
--
--insert into RecurringEvent (id, period, event) values (1, 5, 3);
--insert into RecurringEvent (id, period, event) values (2, 5, 4);
--insert into RecurringEvent (id, period, event) values (3, 5, 5);
--insert into RecurringEvent (id, period, event) values (4, 5, 6);
--insert into RecurringEvent (id, period, event) values (5, 5, 7);
-- END

-- BEGIN Enable this you are using table-per-concrete inhertance mapping strategy --
insert into Event (id, title, description, startTime, endTime, calendarId) values (1, 'Cuti umum', '', '2015-10-20 00:00:00.000', '2015-10-21 00:00:00.000', 2);
insert into Event (id, title, description, startTime, endTime, calendarId) values (2, 'Cuti umum', '', '2015-11-04 00:00:00.000', '2015-11-06 00:00:00.000', 4);

insert into RecurringEvent (id, title, description, startTime, endTime, period, calendarId) values (1, 'Andi''s birthday', '', '1991-04-03 00:00:00.000', '1991-04-03 00:00:00.000', 5, 1);
insert into RecurringEvent (id, title, description, startTime, endTime, period, calendarId) values (2, 'Fidia''s birthday', '', '1988-06-14 00:00:00.000', '1988-06-14 00:00:00.000', 5, 2);
insert into RecurringEvent (id, title, description, startTime, endTime, period, calendarId) values (3, 'Muhammad''s birthday', '', '1989-02-11 00:00:00.000', '1989-02-11 00:00:00.000', 5, 3);
insert into RecurringEvent (id, title, description, startTime, endTime, period, calendarId) values (4, 'Yanti''s birthday', '', '1975-11-05 00:00:00.000', '1975-11-05 00:00:00.000', 5, 4);
insert into RecurringEvent (id, title, description, startTime, endTime, period, calendarId) values (5, 'Zildan''s birthday', '', '1980-09-25 00:00:00.000', '1980-09-25 00:00:00.000', 5, 5);
-- END



-- Credential data
--
-- username : madun, password : sha1(appletree), rights : exercise.user, exercise.admin 
-- username : madun, password : sha1(niceday), rights : exercise.user
--
-- Credential
insert into Sec_User (id, username, password, enabled, accountNotExpired, accountNotLocked, passwordNotExpired) values (1, 'madun', 'fd9853d73fae49eca17264339db6d1d2a54c115b', true, true, true, true);
insert into Sec_User (id, username, password, enabled, accountNotExpired, accountNotLocked, passwordNotExpired) values (2, 'sopo', '0f70e36c17d83bc7089ea244d36a3522de7b2c47', true, true, true, true);
-- Rights
insert into Sec_Right (id, rightName, description) values (1, 'ROLE_ADMIN', '');
insert into Sec_Right (id, rightName, description) values (2, 'ROLE_USER', '');
-- Credential - Rights map
insert into Sec_UserRights (UserId, RightId) values (1, 1);
insert into Sec_UserRights (UserId, RightId) values (1, 2);
insert into Sec_UserRights (UserId, RightId) values (2, 2);


