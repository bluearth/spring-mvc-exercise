<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Person list - Agenda</title>
</head>
<body>
	<h1>Person</h1>
	<form method="post">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<p>
			Name 
			<input type="text" name="name"/> 
			${ bindingResult.getFieldError('name').defaultMessage }
		</p>
		<p>
			Date of birth 
			<input type="date" name="dob"/> 
			${ bindingResult.getFieldError('dob').defaultMessage }
		</p>
		<input type="submit" name="submit" value="Add"/>
	</form>
	<br/>
	<table>
	<thead>
		<tr>
			<td>Id</td>
			<td>Name</td>
			<td>Dob</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${items}" var="item">
		<tr>
			<td><c:if test="${item eq newitem}">*</c:if> ${item.id}</td>
			<td>${item.name}</td>
			<td><fmt:formatDate pattern="yyy-MM-dd" value="${item.dob}"/></td>
		</tr>	
		</c:forEach>
	</table>
</body>
</html>