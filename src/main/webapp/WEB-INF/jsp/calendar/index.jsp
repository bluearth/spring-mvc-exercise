<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Calendar list</title>
</head>
<body>
	<h1>Calendar</h1>
	<table>
	<thead>
		<tr>
			<th>Id</th>
			<th>Title</th>
			<th>Owner</th>
	</thead>
	<tbody>
	<c:forEach var="calendar" items="${calendars}">
		<tr>
			<td>${calendar.id}</td>
			<td>${calendar.title}</td>
			<td>${calendar.owner.name}</td>
		</tr>
	</c:forEach>
	</tbody>
	</table>
	
	
</body>
</html>